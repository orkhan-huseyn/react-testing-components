export function arithmeticMean(array = []) {
  let sum = 0;
  let length = array.length;
  if (length === 0) {
    return 0;
  }

  for (let num of array) {
    sum += num;
  }
  return sum / length;
}
