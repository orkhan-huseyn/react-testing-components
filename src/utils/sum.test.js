import { sum } from "./sum";

describe("sum", () => {
  it("adds positive numbers correctly", () => {
    const result = sum([1, 2, 3, 4, 5]);
    expect(result).toBe(15);
  });

  describe("something else", () => {
    it("adds negative numbers correctly", () => {
      const result = sum([-1, -2, -3, -4, -5]);
      expect(result).toBe(-15);
    });

    it("adds mixed numbers correctly", () => {
      const result = sum([-1, 0, 1, 2, -2]);
      expect(result).toBe(0);
    });
  });

  it("returns zero when input array is empty", () => {
    const result = sum([]);
    expect(result).toBe(0);
  });

  it("returns zero when input array is not present", () => {
    const result = sum();
    expect(result).toBe(0);
  });
});
