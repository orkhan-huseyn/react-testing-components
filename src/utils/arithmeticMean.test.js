import { arithmeticMean } from "./arithmeticMean";

describe("arithmeticMean", () => {
  it("works fine with the same number", () => {
    const result = arithmeticMean([3, 3, 3, 3, 3]);
    expect(result).toBe(3);
  });

  it("works fine with the miex numbers", () => {
    const result = arithmeticMean([1, 5, 6, 10, 4]);
    expect(result).toBe(5.2);
  });

  it("returns zero with empty array", () => {
    const result = arithmeticMean([]);
    expect(result).toBe(0);
  });

  it("returns zero when input array is not present", () => {
    const result = arithmeticMean();
    expect(result).toBe(0);
  });
});
