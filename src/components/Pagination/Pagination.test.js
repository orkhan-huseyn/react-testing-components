import { fireEvent, render, screen } from "@testing-library/react";
import Pagination from "./index";

describe("Pagination", () => {
  it("should not show anything if `total` prop is zero", () => {
    render(<Pagination />);
    const nav = screen.queryByRole("navigation");
    expect(nav).not.toBeInTheDocument();
  });

  it("should show one page when `total` prop is more than zero", () => {
    render(<Pagination total={5} />);
    const listItems = screen.queryAllByRole("listitem");
    expect(listItems).toHaveLength(3);
  });

  it("should display 2 pages when `total` is double of `pageSize`", () => {
    render(<Pagination total={10} pageSize={5} />);
    const listItems = screen.queryAllByRole("listitem");
    expect(listItems).toHaveLength(4);
  });

  it("should display arrows in prev and next button labels", () => {
    render(<Pagination total={10} pageSize={5} />);
    const prevButton = screen.queryByText(/<</);
    const nextButton = screen.queryByText(/>>/);
    expect(prevButton).toBeInTheDocument();
    expect(nextButton).toBeInTheDocument();
  });

  it("should call onChange callback when page is changed", () => {
    const onPageChangeCallback = jest.fn();
    render(
      <Pagination total={10} pageSize={5} onPageChange={onPageChangeCallback} />
    );
    const secondPageButton = screen.queryByText(/2/);
    fireEvent.click(secondPageButton);
    expect(onPageChangeCallback).toHaveBeenCalled();
  });

  it("page number should be active when clicked", () => {
    render(<Pagination total={10} pageSize={5} />);
    const secondPageButton = screen.queryByText(/2/);
    fireEvent.click(secondPageButton);
    expect(secondPageButton.parentElement).toHaveClass("active");
  });
});
